## 1.Document Object Model
DOM представляет html в виде конструктора (блоков), каждый элемент в виде объекта
присутствует такая иерархия (дерерво, где все начинается с html):

                         HTML
                /                 \
           HEAD                   BODY
         /  |     \         /      |       \
    title   meta  link    header  main    footer
    |       |      |      / | \    /|\       /|\
#text     #text   #text   ....................

## 2. Разница между  innerHTML и innerText
// <p> Hallo  <b> World! </b> </p>
1)innerHTML показывает значение в виде строки (сам текст-контент и вложенные теги, прпимер: Hallo  <b> World! </b>  )
так же можно перезаписатьо текст с тегами (и без них) и они тоже будут работать 

2) innerText "считывает" только текстовый контент (пример: Hallo World! )


## Обртатиться к элементу JS можно:
document.getElementById('id') - элемаент по id атрибуту
document.getElementsById('id') - коллекция по id атрибуту
document.querySelectorAll ('.class') - коллекция всех элементов по классу
document.querySelector ('.class')/("#id") - первый элемент, который имеет данный .класс/ #id 
document.getElementsByName ('name') - коллекция, заданная по "name"
document.getElementsByTagNamer ('tag') - коллекция по тегу (все теги по заданному)
document.getElementsByClassName ('class') - коллекция по класу 