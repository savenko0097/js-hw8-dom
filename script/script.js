"use strict";

console.log(document.head);

// 1. change background-colors for "p"

const paragraph = document.getElementsByTagName("p");
for (let elem of paragraph) {
    elem.style.backgroundColor = '#ff0000';
}


// 2. 
const optionsList = document.getElementById("optionsList");
console.log(optionsList);

    //parents
const parent = optionsList.parentElement;
console.log(parent);

    // children
if(optionsList.children!==null){
    console.log(optionsList.children);
} else{
    console.log("no children");
}


//3. Change text 

const testParagraph = document.getElementById("testParagraph");
testParagraph.innerHTML = "<p>This is a paragraph<p/>";



//4. Find all "li" in class="main-header" + console  && change class-name

const mainHeader = document.querySelector(".main-header");
const mainHeaderList = mainHeader.getElementsByTagName("li");
console.log(mainHeaderList);

for (const items of mainHeaderList) {
    items.classList.add("nav-item");
}
console.log(mainHeaderList);

//5. find class="section-title" and delete it

const sectionTitle = document.querySelectorAll('.section-title');
console.log(sectionTitle);

for (const headerTitle of sectionTitle) {
    headerTitle.classList.remove("section-title");
    console.log(headerTitle);
}
